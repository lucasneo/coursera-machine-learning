---
title: 'Activity Quality Prediction'
author: "Lucas Neo"
date: "15 September 2015"
output: html_document
---
  
### Libraries

```r
library(ggplot2)
library(caret)
library(doParallel)
```

Set the seed for reproducibility.

```r
set.seed(2102)
```
<br>
  
### Data Preprocessing
The data was downloaded from this url on 15th September 2015: https://d396qusza40orc.cloudfront.net/predmachlearn/pml-training.csv  
For more information on this dataset, go to this link http://groupware.les.inf.puc-rio.br/har.  
<br>

#### Loading Data

```r
train = read.csv('https://d396qusza40orc.cloudfront.net/predmachlearn/pml-training.csv')
test = read.csv('https://d396qusza40orc.cloudfront.net/predmachlearn/pml-testing.csv')
```
<br>
  
#### Data Cleaning
Identify columns that are NA or blank in the test dataset.

```r
whitespace = sapply(test, function(x) any(x == "" | is.na(x)))
no_whitespace = !whitespace
```
<br>

Subset the test dataset.

```r
test_clean = test[,names(test)[no_whitespace]]
test_clean = test_clean[,-(1:7)]
```
<br>

Subset the training dataset to the necessary columns and change `classe` into a factor level variable.

```r
train_clean = train[,names(train)[no_whitespace]]
train_clean = train_clean[,-(1:7)]

train_clean$classe = factor(train_clean$classe)
```
<br>
  
#### Data Slicing
Perform cross validation by splitting the training dataset into 70% for training and 30% for validation.

```r
train_random = createDataPartition(train_clean$classe, p=0.7, list=F)
train_train = train_clean[train_random,]
train_test = train_clean[-train_random,]
```
<br>
  
### Data Modeling
Random forest was chosen as it is a highly accurate classification method which overcomes the habit for decision trees to overfit the data. Here, the model classifies activity quality based on activity metrics gained from sensor data.
<br>

#### Create Clusters to Use Multiple Cores

```r
cluster = makeCluster(detectCores() - 1)
registerDoParallel(cluster)
```
<br>

#### Train the Model
Train the model against the training partition of the training dataset.

```r
rf_model = train(classe ~., data=train_train, method='rf')
```

```
## Loading required package: randomForest
## randomForest 4.6-10
## Type rfNews() to see new features/changes/bug fixes.
```

```r
stopCluster(cluster)
```
<br>

#### Cross Validate the Model
Test it against the test partition of the training dataset.

```r
training = predict(rf_model,train_test)
confusionMatrix(train_test$classe,training)
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction    A    B    C    D    E
##          A 1673    1    0    0    0
##          B    4 1134    1    0    0
##          C    0    6 1016    4    0
##          D    0    1   12  951    0
##          E    0    1    4    1 1076
## 
## Overall Statistics
##                                           
##                Accuracy : 0.9941          
##                  95% CI : (0.9917, 0.9959)
##     No Information Rate : 0.285           
##     P-Value [Acc > NIR] : < 2.2e-16       
##                                           
##                   Kappa : 0.9925          
##  Mcnemar's Test P-Value : NA              
## 
## Statistics by Class:
## 
##                      Class: A Class: B Class: C Class: D Class: E
## Sensitivity            0.9976   0.9921   0.9835   0.9948   1.0000
## Specificity            0.9998   0.9989   0.9979   0.9974   0.9988
## Pos Pred Value         0.9994   0.9956   0.9903   0.9865   0.9945
## Neg Pred Value         0.9991   0.9981   0.9965   0.9990   1.0000
## Prevalence             0.2850   0.1942   0.1755   0.1624   0.1828
## Detection Rate         0.2843   0.1927   0.1726   0.1616   0.1828
## Detection Prevalence   0.2845   0.1935   0.1743   0.1638   0.1839
## Balanced Accuracy      0.9987   0.9955   0.9907   0.9961   0.9994
```

```r
out_of_sample = 1 - confusionMatrix(train_test$classe,training)$overall[1]
out_of_sample
```

```
##    Accuracy 
## 0.005947324
```
From the confusion matrix, this model has a 99.41% accuracy and 0.59% out of sample rate.
<br>

### Prediction
Using the trained model to predict activity classifications on the test dataset.

#### Test Data Prediction
Run the prediction while ignoring the `problem id` variable.

```r
prediction = predict(rf_model,test_clean[,-length(names(test_clean))])
prediction
```

```
##  [1] B A B A A E D B A A B C B A E E A B B B
## Levels: A B C D E
```
